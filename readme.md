# Feature Point Tracker 

This repository contains the solution to the 1st Proyect for the Compuver Vision course in the Electronics Master in the ITCR.

## Requirements

opencv >= 4.4.0
numpy

## Project structure

```
├── docs # Contains paper, still in progress
│   ├── data
│   │   ├── akaze_aep.csv
│   │   ├── akaze_comparision.csv
│   │   ├── sift_aep.csv
│   │   └── sift_comparision.csv
│   ├── literature.bib
│   ├── macros.tex
│   ├── Makefile
│   └── proyecto_emmanuel_madrigal.tex
├── LICENSE
├── readme.md # This file
└── src
    ├── download_test_dataset.sh # Downloads the Oxford Affine Transform dataset
    ├── main.py # Main source file
```

## Usage

### Get help
```
./main 
./main -h
```

### Detect and track based on an image using the camera
```
./main -f reference.png -v
./main -f reference.png --camera_capture
```

### Capture a reference image set of files
```
./main -f reference.png -t images_path_filename
./main -f reference.png --image_names_file images_path_filename
```

### Selecting a descriptor and matcher
```
./main {other options from the other commands} --descriptor sift --matcher flann
./main {other options from the other commands} -d sift -m bruteforce
```

Descriptors:
* sift
* akaze
* none

Matcher:
* bruteforce
* flann
* none

### Capture a reference image from camera

```
./main --save_filename reference_filename.png -v
./main -c reference_filename.png -v
```

This will save a single image named `reference_filename.png` whenever you press the spacebar.

After capturing a frame, the feature detection will start tracking from that image.

It is recommended that this command is used with `-d none -m none` to avoid a tracking square from being shown.

### Capture a set of images from camera

```
./main --save_dir images_path -v
./main -r images_path -v
```

This will save images on `images_path` and create an `images_path.txt` with the names of the saved filenames.

It is recommended that this command is used with `-d none -m none` to avoid a tracking square from being shown.