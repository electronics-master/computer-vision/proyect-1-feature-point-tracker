\documentclass{IEEEconf}

\input{macros}

\author{Emmanuel Madrigal}

\title{Evaluation of feature descriptors and matching}

\begin{document}

\maketitle

\begin{abstract}
 This paper presents the comparison between the AKAZE and the SIFT feature descriptors and compares
 them in different conditions. It also measures the amount of of descriptors that each of this is able to manage and the processing stages for each of the stages. Finally it also compares the error in the determined matched descriptors against the ground-truth homgraphy.
\end{abstract}

\section{Introduction}

Image point feature tracking in video sequences is a key component in many
computer vision problems, such as structure from motion and object
detection/recognition. 

Feature detection and description is a necessary step in many computer vision
algorithms. Distinctive image features can be used to establish matches across
multiple images in a video sequence. These matches can be used to carry out many
computer vision tasks, such as the estimation of camera motion parameters,
structure from motion, and object detection/recognition~/cite{sargent2009feature}.

The challenge is how to obtain robust features under variable
illumination, background changes, camera motion and zooming, viewpoint
changes, partial occlusions, geometric and photometric variations of
objects~\cite{shao2010feature}.


Trackers and detectors operate by:
\begin{enumerate}
 \item Detecting feature locations and characteristics.
 \item Describing each feature with local image information.
 \item Associating features between image.
\end{enumerate}

\subsection{Feature Detection}
The first step is feature detection.
Corner~\cite{harris1988combined}\cite{shi1994good} and
blob~\cite{lindeberg1998feature} detectors are commonly used to identify salient
features within images for tracking purposes.

\subsection{Feature Description}

After a feature has been detected, a description of that feature is extracted
from its local region.

\subsubsection{SIFT}

The SIFT detector~\cite{lowe2004distinctive} is based on Difference-of-Gaussians (DoG)operator which is an 
approximation of Laplacian-of-Gaussian (LoG). Feature-points are
detected by searching local maxima using DoG at various scales of the
subject images. The description method extracts a $16\times 16$ neighborhood
around each detected feature and further segments the region into
sub-blocks, rendering a total of 128 bin values. SIFT is robustly
invariant to image rotations, scale, and limited affine variations but
its main drawback is high computational cost. Equation \ref{eq:sift} shows the
convolution of difference of two Gaussians (computed at different scales)
with image $\mathbf{I} \left( \mathbf{x}, \mathbf{y}\right)$.

\begin{equation}
 \mathbf{D} \left( x, y, \sigma \right) = \left( \mathbf{G} \left( x, y, k\sigma \right) - \mathbf{G} \left( x, y, \sigma \right) \right) * \mathbf{I} \left( x, y \right)
 \label{eq:sift}
\end{equation}

Where $G$ represents the Gaussian function


\subsubsection{KAZE}

KAZE features~\cite{alcantarilla2012kaze} exploit non-linear scale space
through non-linear diffusion filtering. This makes blurring in images locally
adaptive to feature-points, thus reducing noise and simultaneously
retaining the boundaries of regions in subject images. KAZE detector is
based on scale normalized determinant of Hessian Matrix which is computed
at multiple scale levels. The maxima of detector response are picked up as
feature-points using a moving window. Feature description introduces the
property of rotation invariance by finding dominant orientation in
a circular neighborhood around each detected feature. KAZE features are
invariant to rotation, scale, limited affine and have more
distinctiveness at varying scales with the cost of moderate increase in
computational time. Equation \ref{eq:kaze} shows the standard nonlinear diffusion
formula. 

\begin{equation}
 \frac{\partial L}{\partial t} = div \left( c \left( x, y, t\right). \nabla L \right)
 \label{eq:kaze}
\end{equation}

Where $c$ is conductivity function, $div$ is divergence, $\nabla$ is
gradient operator and $L$ is image luminance.

\subsubsection{AKAZE}

Accelerated-KAZE~\cite{alcantarilla2011fast} is also based on non-linear
diffusion filtering like KAZE but its non-linear scale spaces are constructed
using a computationally efficient framework called Fast Explicit Diffusion
(FED)~\cite{weickert2016cyclic}~\cite{grewenig2010box}. The AKAZE detector is based on the determinant of Hessian Matrix.
Rotation invariance quality is improved using Scharr filters~\cite{scharr2004optimal}. Maxima of the
detector responses in spatial locations are picked up as feature-points.
Descriptor of AKAZE is based on Modified Local Difference Binary (MLDB)
algorithm which is also highly efficient. AKAZE features are invariant to scale,
rotation, limited affine and have more distinctiveness at varying scales because
of nonlinear scale spaces.

\subsection{Feature Matching}

Feature association is an expensive operation for trackers. The simplest
solution has a complexity of $O(N^2)$, although faster approximate alternatives
are available(e.g. k-d trees~\cite{silpa2008optimised}). An optimal solution is
defined as the two features that minimize a distance metric, often the $L_2$ or
the $L_1$ norm for \textit{string based descriptors} such as SIFT, or the
\textit{Hamming distance} for binary descriptors such as AKAZE.

\subsection{Average Endpoint Error}

End-to-end point error is an optical flow error measurement calculated by
comparing an estimated optical flow vector ($V_{est}$ ) against with a
ground-truth optical flow vector ( $V_{gt}$ ).

\begin{equation}
    EPE = ||  V_{est}  - V_{gt}||  
\end{equation}

In order to apply this to measure the quality of the detectors, the vectors are defined as follows:

\begin{equation}
    V_{est} = P_{dest} - P_{orig}
\end{equation}

\begin{equation}
    V_{est} = H_{gt} P_{orig} - P_{orig}
\end{equation}

Where $H_{gt}$ is the ground truth homography, $P_{orig}$ is a point in the reference image, while $P_{dest}$ is a point in the target image. Only those matched points and considered ``good'' matches are used for this calculation.

Since both use $P_{orig}$ the equation is simplified as follows:

\begin{multline}
    \overline{EPE} = \frac{1}{N} \sum_{i} || H_{gt} P_{orig} - P_{dest} || \quad i \in \text{good matches} \\ N = len(\text{good matches})
\end{multline}

\section{Results}

Comparisions are made using the Affine Covariant Features Datasets~\cite{visual_geometry_group}. These include different sets of $765\times 512$ images representing different transformations:

\begin{itemize}
    \item Bikes: Varying blur.
    \item Trees: Varying blur.
    \item Graffiti: Varying viewpoint.
    \item Wall: Varying viewpoint.
    \item Bark: Varying blur, changing zoom and rotation.
    \item Boat: Varying blur, changing zoom and rotation.
    \item Leuven: Varying blur, changing light.
    \item UBC: Varying JPEG Compression.
\end{itemize}

On tables~\ref{tab:sift} and~\ref{tab:akaze} comparison tables are shown for the
time required for each of the different stages, and characteristics on the
results from each of this stages. This results are averaged for each of the pair
of images on the dataset where the homography was successfully calculated.

The SIFT descriptor is able to detect and manages to describe around twice the
number of the descriptors in around twice the time. The number of matched
descriptors also exhibits a similar behavior while using the FLANN matcher in
both cases.

However the calculation time for the homography is almost the same in both
cases, which is expected since both of these are using a RANSAC algorithm.

\begin{table*}[htbp]
 \begin{adjustbox}{width=\linewidth}
 \begin{tabular}{|c|c|c|c|c|c|c|c|}%
 \hline 
 Set & Detect Features & Detected Features Time (ms) & Matched Features & Matched Features Time (ms) & Inliers & Calculated Inliers Time (ms) & Homography Calculation Time (ms)
 \csvreader[head to column names]{data/sift_comparision.csv}{}% use head of csv as column names
 {\\\hline\Set & \detectedFeatures\ & \detectedFeaturesTime & \matchedFeatures & \matchedFeaturesTime & \rejectedOutliers & \rejectedOutliersTime & \homographyTime}% Columns
 \\\hline 
 \end{tabular}
 \end{adjustbox}
 \caption{Comparison table for SIFT descriptor}
 \label{tab:sift}
\end{table*}


\begin{table*}[htbp]
 \begin{adjustbox}{width=\linewidth}
 \begin{tabular}{|c|c|c|c|c|c|c|c|}%
 \hline 
 Set & Detect Features & Detected Features Time (ms) & Matched Features & Matched Features Time (ms) & Inliers & Calculated Inliers Time (ms) & Homography Calculation Time (ms)
 \csvreader[head to column names]{data/akaze_comparision.csv}{}% use head of csv as column names
 {\\\hline\Set & \detectedFeatures\ & \detectedFeaturesTime & \matchedFeatures & \matchedFeaturesTime & \rejectedOutliers & \rejectedOutliersTime & \homographyTime}% Columns
 \\\hline 
 \end{tabular}
 \end{adjustbox}
 \caption{Comparision table for Akaze descriptor}
 \label{tab:akaze}
\end{table*}

On figures~\ref{fig:akaze} and~\ref{fig:sift} the average endpoint error for the
different images can be seen. The maximum values for the endpoint error is
larger in the case of the SIFT descriptor, however it should be noted that the
AKAZE descriptor has more cases where it is unable to calculate the homography.
Also, although the values might be larger, the homography changes in this cases
represent a difference in pixels, so this is showing that whenever the
homography is calculated, the calculated homography doesn't vary more than a fex
pixels.

The dataset where both algorithms failed to calculate the homography value is
the \textit{graffiti}, this includes changes in viewpoint and the AKAZE
algorithm failed to obtain the homography in more one more image pair. This is
also the case for some images in the \textit{wall} dataset which also includes
varying viewpoints, and in this case the AKAZE also has one more failure to
calculate the homography than SIFT.

All other transformations are correctly calculated, which include varying
degrees of blurring, zooming, rotation, changes in lighting and JPEG compression
artifacts.


\begin{figure}[htbp]
 \begin{tikzpicture}
 \begin{axis}[legend pos=outer north east]
 \addplot table [x=Image, y=Bark, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{Bark}
 \addplot table [x=Image, y=Bike, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{Bike}
 \addplot table [x=Image, y=Boat, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{Boat}
 \addplot table [x=Image, y=Graf, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{Graffiti}
 \addplot table [x=Image, y=Leuven, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{Leuven}
 \addplot table [x=Image, y=Trees, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{Trees}
 \addplot table [x=Image, y=UBC, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{UBC}
 \addplot table [x=Image, y=Wall, col sep=comma] {data/sift_aep.csv};
 \addlegendentry{Wall}
 \end{axis}
 \end{tikzpicture}
 \caption{Average endpoint error for SIFT algorithm}
 \label{fig:sift}
\end{figure}

\begin{figure}[htbp]
 \begin{tikzpicture}
 \begin{axis}[legend pos=outer north east]
 \addplot table [x=Image, y=Bark, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{Bark}
 \addplot table [x=Image, y=Bike, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{Bike}
 \addplot table [x=Image, y=Boat, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{Boat}
 \addplot table [x=Image, y=Graf, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{Graffiti}
 \addplot table [x=Image, y=Leuven, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{Leuven}
 \addplot table [x=Image, y=Trees, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{Trees}
 \addplot table [x=Image, y=UBC, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{UBC}
 \addplot table [x=Image, y=Wall, col sep=comma] {data/akaze_aep.csv};
 \addlegendentry{Wall}
 \end{axis}
 \end{tikzpicture}
 \caption{Average endpoint error for AKAZE algorithm}
 \label{fig:akaze}
\end{figure}

\section{Conclusions}

The SIFT algorithm is able to get a larger number of descriptors than the AKAZE, and this allows for bigger number of good matches to be obtained which translates into more successful homographies.

Both algorithms are able to correctly determine homographies under varying amounts of blur, zoom, rotation, changes in lightning and JPEG compression artifacts. However both have larger issues when changes in the viewpoint are presented.

Neither algorithm is being processed under 33\,ms which means that neither of this could be ran at what is considered real-time.

A more extensive comparison should be made between both of these to include more
algorithms, and to correctly quantify which transformations present the larger
problems to the each of these algorithms.

\printbibliography

\newpage
\section*{Appendix}

\subsection{Qualitative results}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bark/akaze/img5.png}
    \end{subfigure}
    \caption{Bark Database, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/bikes/akaze/img5.png}
    \end{subfigure}
    \caption{Bikes Dataset, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/boat/akaze/img5.png}
    \end{subfigure}
    \caption{Boat Dataset, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/graf/akaze/img5.png}
    \end{subfigure}
    \caption{Graf Dataset, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/leuven/akaze/img5.png}
    \end{subfigure}
    \caption{Leuven Dataset, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/trees/akaze/img5.png}
    \end{subfigure}
    \caption{Trees Dataset, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/ubc/akaze/img5.png}
    \end{subfigure}
    \caption{UBC Dataset, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\begin{figure*}
    \centering
    \begin{subfigure}{.50\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/img0.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/sift/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/sift/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/sift/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/sift/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/sift/img5.png}
    \end{subfigure}
    \newline
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/akaze/img1.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/akaze/img2.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/akaze/img3.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/akaze/img4.png}
    \end{subfigure}%
    \begin{subfigure}{.20\textwidth}
      \centering
      \includegraphics[width=\linewidth]{figures/wall/akaze/img5.png}
    \end{subfigure}
    \caption{Wall Dataset, 1st row is the reference image, in the 2nd row are the SIFT results and in the 3rd row are the AKAZE results}
\end{figure*}

\end{document}

