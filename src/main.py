#! /usr/bin/env python3

from abc import ABC, abstractmethod
import csv
import os
import pathlib
import sys
import time

import argparse
import cv2
import numpy as np

NN_MATCH_RATIO = 0.5  # Lower is more strict
IMAGE_WAIT_TIME = 100  # ms

RUN_PROFILING = False
DETECTED_FEATURES_COUNT = []
MATCHED_FEATURES_COUNT = []
REJECTED_OUTLIERS_COUNT = []
DETECT_FEATURES_TIME = []
MATCHED_FEATURES_TIME = []
REJECTED_OUTLIERS_TIME = []
HOMOGRAPHY_TIME = []
IMG_COUNTER = 1

AVERAGE_ENDPOINT_ERROR = []

DESCRIPTOR_TO_DTYPE = {
    "sift": np.float32,
    "akaze": np.uint8
}


class Descriptor(ABC):
    """
    Descriptor parent class
    """

    def get_keypoints_and_descriptors(self, image):
        """
        Returns the keypoints and the descriptors for a given image

        Args:
            image (np.array): Input image

        Returns:
            (list(cv2.Keypoint), np.array): [description]
        """
        global RUN_PROFILING

        if RUN_PROFILING:
            start = time.time()

        keypoints, descriptors = self.descriptor.detectAndCompute(image, None)

        if RUN_PROFILING:
            end = time.time()
            print("description elapsed time: ", end - start)
            print("number descriptors: ", len(descriptors))

            global DETECT_FEATURES_TIME
            global DETECTED_FEATURES_COUNT
            DETECT_FEATURES_TIME.append(end - start)
            DETECTED_FEATURES_COUNT.append(len(descriptors))

        return keypoints, descriptors


class SIFT_Descriptor(Descriptor):
    """Class implementing the SIFT descritor

    Args:
        Descriptor (Descriptor): Parent descriptor class implementing the detection and computing
    """

    def __init__(self):
        """Initializes the OpenCV SIFT class
        """
        self.descriptor = cv2.SIFT_create()


class AKaze_Descriptor(Descriptor):
    """Class implementing the AKAZE descriptor

    Args:
        Descriptor (Descriptor): Parent descriptor class implementing the detection and computing
    """

    def __init__(self):
        """Initializes the OpenCV AKAZE class
        """
        self.descriptor = cv2.AKAZE_create()


class Matcher(ABC):
    """Matcher parent class
    """

    @abstractmethod
    def preprocess_descriptor(self, descriptor):
        """Preprocess descriptor

        This abstract method should be reimplemented by each

        Args:
            descriptor (np.array): descriptor to process

        Returns:
            Reimplemented method should return the descriptor with any neccesary preprocessing
        """

    def match(self, desc1, desc2):
        """Matches the descriptors and returns the "Good matches"

        Args:
            desc1 (np.array): First descriptor
            desc2 (np.array): Second descriptor

        Returns:
            list(cv2.DMatch): List of good matches between desc1 and desc2
        """
        global RUN_PROFILING
        desc1 = self.preprocess_descriptor(desc1)
        desc2 = self.preprocess_descriptor(desc2)

        if RUN_PROFILING:
            start = time.time()
        nn_matches = self.matcher.knnMatch(desc1, desc2, 2)
        if len(nn_matches[0]) != 2:
            return []

        if RUN_PROFILING:
            end = time.time()
            print("matching elapsed time: ", end - start)
            print("number matches: ", len(nn_matches))

            global MATCHED_FEATURES_COUNT
            global MATCHED_FEATURES_TIME
            MATCHED_FEATURES_TIME.append(end - start)
            MATCHED_FEATURES_COUNT.append(len(nn_matches))

        if RUN_PROFILING:
            start = time.time()
        good_matches = []
        for m, n in nn_matches:
            if m.distance < NN_MATCH_RATIO * n.distance:
                good_matches.append(m)

        if RUN_PROFILING:
            end = time.time()
            print("outlier rejection elapsed time: ", end - start)
            print("number good matches: ", len(good_matches))

            global REJECTED_OUTLIERS_COUNT
            global REJECTED_OUTLIERS_TIME
            REJECTED_OUTLIERS_TIME.append(end - start)
            REJECTED_OUTLIERS_COUNT.append(len(good_matches))

        return good_matches


class FlannMatcher(Matcher):
    """Class implementing the FLANN matcher

    Args:
        Matcher (Matcher): Parent matcher class implementing the matching
    """

    def __init__(self, descriptor_name):
        """Initializes the FLANN descritor

        Args:
            descriptor_name (str): Name of the descriptor
        """
        self.matcher = cv2.DescriptorMatcher_create(
            cv2.DescriptorMatcher_FLANNBASED)
        self.descriptor_dtype = DESCRIPTOR_TO_DTYPE[descriptor_name]

    def preprocess_descriptor(self, descriptor):
        """Preprocess a descriptor

        Flann needs the descriptors in float32 format, if the format
        is different it will reformat the descriptor to the correct format

        Args:
            descriptor (np.array): Descriptor to preprocess

        Returns:
            np.array: Descriptor in float32 format
        """
        return descriptor.astype(np.float32)


class BFMatcher(Matcher):
    """Class implementing the Brute Force matcher

    Args:
        Matcher (Matcher): Parent matcher class implementing the matching
    """

    def __init__(self, descriptor_name):
        """Initializes the Brute force matcher

        This function will instantiate either a Hamming or an L1
        brute force matcher according to the descriptor needs.

        Args:
            descriptor_name (str): Name of the descriptor
        """
        descriptor_dtype = DESCRIPTOR_TO_DTYPE[descriptor_name]

        if np.float32 == descriptor_dtype:
            self.matcher = cv2.DescriptorMatcher_create(
                cv2.DescriptorMatcher_BRUTEFORCE_L1)
        elif np.uint8 == descriptor_dtype:
            self.matcher = cv2.DescriptorMatcher_create(
                cv2.DescriptorMatcher_BRUTEFORCE_HAMMING)

    def preprocess_descriptor(self, descriptor):
        """Preprocess the descriptor for the brute force matcher

        No preprocessing is applied, the matcher is initialized to
        according the the descriptor data type

        Args:
            descriptor (np.array): Descriptor to preprocess

        Returns:
            np.array: Returns the descriptor as is
        """
        return descriptor


def get_homography(keypoints_obj, keypoints_scene, good_matches):
    """Obtains the homography based on the scene and object keypoints and the matches between them

    Args:
        keypoints_obj (list(cv2.Keypoint)): List of keypoints from the object
        keypoints_scene (list(cv2.Keypoint)): List of keypoints from the scene
        good_matches (list(cv2.DMatch)): List of good matches

    Returns:
        np.array: 3x3 matrix with the calculated homography
    """
    global RUN_PROFILING
    if RUN_PROFILING:
        start = time.time()
    # -- Localize the object
    obj = np.empty((len(good_matches), 2), dtype=np.float32)
    scene = np.empty((len(good_matches), 2), dtype=np.float32)
    for i, match in enumerate(good_matches):
        # -- Get the keypoints from the good matches
        obj[i, 0] = keypoints_obj[match.queryIdx].pt[0]
        obj[i, 1] = keypoints_obj[match.queryIdx].pt[1]
        scene[i, 0] = keypoints_scene[match.trainIdx].pt[0]
        scene[i, 1] = keypoints_scene[match.trainIdx].pt[1]

    homography, _ = cv2.findHomography(obj, scene, cv2.RANSAC)

    if RUN_PROFILING:
        end = time.time()
        print("homography elapsed time: ", end - start)

        global HOMOGRAPHY_TIME
        HOMOGRAPHY_TIME.append(end - start)

    return homography


def draw_homography(reference_image, output_image, homography):
    """
    Draws the homography over the destination image

    This function will draw a rectangle that corresponds to the
    reference image in the calculated position in the destination image.

    Args:
        reference_image (np.array): Reference image
        output_image (np.array): Ouput image where the homography will be drawn
        H (np.array): 3x3 array with the homography
    """
    # -- Get the corners from the image_1 ( the object to be "detected" )
    obj_corners = np.empty((4, 1, 2), dtype=np.float32)
    obj_corners[0, 0, 0] = 0
    obj_corners[0, 0, 1] = 0
    obj_corners[1, 0, 0] = reference_image.shape[1]
    obj_corners[1, 0, 1] = 0
    obj_corners[2, 0, 0] = reference_image.shape[1]
    obj_corners[2, 0, 1] = reference_image.shape[0]
    obj_corners[3, 0, 0] = 0
    obj_corners[3, 0, 1] = reference_image.shape[0]

    scene_corners = cv2.perspectiveTransform(obj_corners, homography)

    # -- Draw lines between the corners (the mapped object in the scene - image_2 )
    cv2.line(output_image, (int(scene_corners[0, 0, 0]), int(scene_corners[0, 0, 1])), (int(
        scene_corners[1, 0, 0]), int(scene_corners[1, 0, 1])), (0, 255, 0), 4)
    cv2.line(output_image, (int(scene_corners[1, 0, 0]), int(scene_corners[1, 0, 1])), (int(
        scene_corners[2, 0, 0]), int(scene_corners[2, 0, 1])), (0, 255, 0), 4)
    cv2.line(output_image, (int(scene_corners[2, 0, 0]), int(scene_corners[2, 0, 1])), (int(
        scene_corners[3, 0, 0]), int(scene_corners[3, 0, 1])), (0, 255, 0), 4)
    cv2.line(output_image, (int(scene_corners[3, 0, 0]), int(scene_corners[3, 0, 1])), (int(
        scene_corners[0, 0, 0]), int(scene_corners[0, 0, 1])), (0, 255, 0), 4)


def get_reference_image(reference_image_filename):
    """Reads a file form local storage and returns it loaded into memory

    Args:
        reference_image_filename (str): Filename of the image to load

    Returns:
        np.array: Returns the image if loaded or None if it failed
    """
    reference_image = None

    if reference_image_filename:
        reference_image = cv2.imread(reference_image_filename)

        if reference_image is None:
            print("Couldn't open " + reference_image_filename +
                  " using first image from sequence as reference")

    return reference_image


class FeatureTracker():
    """Wrapper class for the feature tracking
    """

    def __init__(self, descriptor_name, matcher_name):
        """Constructor for the Feature Tracker

        Initializes the correct descriptor and matcher

        Args:
            descriptor_name (str): Descriptor name
            matcher_name (str): Matcher name
        """
        if descriptor_name == "sift":
            self.descriptor = SIFT_Descriptor()
        elif descriptor_name == "akaze":
            self.descriptor = AKaze_Descriptor()

        if matcher_name == "bruteforce":
            self.matcher = BFMatcher(descriptor_name)
        elif matcher_name == "flann":
            self.matcher = FlannMatcher(descriptor_name)

        self.keypoints_obj = None
        self.descriptors_obj = None

    def add_reference_image(self, reference_image):
        """Calculates keypoints and descriptors for the reference image and stores it

        Args:
            reference_image (np.array): Reference image
        """
        self.keypoints_obj, self.descriptors_obj = self.descriptor.get_keypoints_and_descriptors(
            reference_image)
        self.reference_image = reference_image

    def get_homography(self, scene_image, reference_homography):
        """Returns the homography corresponding to the transform between the reference image and the scene image

        Args:
            scene_image (np.array): Image where the object will be found

        Returns:
            np.array: 3x3 homography matrix if found
        """
        if (self.keypoints_obj is None) or (self.descriptors_obj is None):
            print("Please initialize the reference image before calling this function")
            return

        if scene_image is not None:
            keypoints_scene, descriptors_scene = self.descriptor.get_keypoints_and_descriptors(
                scene_image)
        else:
            print("Please add provide an image")
            return

        if descriptors_scene is not None:
            good_matches = self.matcher.match(
                self.descriptors_obj, descriptors_scene)

            if len(good_matches) < 4:
                print("Too few matches, unable to calculate homography")
            else:
                homography = get_homography(
                    self.keypoints_obj, keypoints_scene, good_matches)

                if reference_homography is not None:
                    origin_points = np.empty(
                        (len(good_matches), 1, 2), np.float)
                    dest_points = np.empty((len(good_matches), 1, 2), np.float)
                    for i, match in enumerate(good_matches):
                        origin_points[i] = self.keypoints_obj[match.queryIdx].pt
                        dest_points[i] = keypoints_scene[match.trainIdx].pt

                    calculated_dest_points = dest_points
                    reference_dest_points = cv2.perspectiveTransform(
                        origin_points, reference_homography)

                    global AVERAGE_ENDPOINT_ERROR
                    AVERAGE_ENDPOINT_ERROR.append(
                        np.average(
                            np.sqrt(
                                np.sum(
                                    np.square(
                                        calculated_dest_points -
                                        reference_dest_points),
                                    axis=2))))
                    print(
                        "Average endpoint error: ",
                        np.average(
                            np.sqrt(
                                np.sum(
                                    np.square(
                                        calculated_dest_points -
                                        reference_dest_points),
                                    axis=2))))

                return homography


def directory_read(
        image_names_file,
        reference_image_filename,
        descriptor_name,
        matcher_name,
        homography_list):
    """Reads all the files from a directory and processes them

    Args:
        image_names_file (str): File with the name of an image on each line
        reference_image_filename ([type]): Filename for the reference image
    """
    reference_image = get_reference_image(reference_image_filename)

    if (descriptor_name != "none") and (matcher_name != "none"):
        feature_tracker = FeatureTracker(descriptor_name, matcher_name)
    else:
        feature_tracker = None

    if reference_image is not None:
        feature_tracker.add_reference_image(reference_image)

    with open(image_names_file, "r") as name_file:
        for i, file_name in enumerate(name_file.readlines()):
            file_name = file_name.strip('\n')
            frame = cv2.imread(file_name)

            if homography_list:
                with open(homography_list, "r") as homography_list_file:
                    homography_file_name = homography_list_file.readlines()[i]
                    homography_file_name = homography_file_name.strip('\n')

                    with open(homography_file_name, "r") as homography_file:
                        reference_homography_csv = csv.reader(
                            homography_file, delimiter=' ')
                        reference_homography = np.empty((3, 3), np.float)

                        for j, row in enumerate(reference_homography_csv):
                            row = list(filter(('').__ne__, row))
                            for k, element in enumerate(row):
                                reference_homography[j, k] = float(element)
            else:
                reference_homography = None

            if frame is None:
                print("Unable to open image: ", file_name)
                continue

            if reference_image is None:
                reference_image = frame

            if reference_image is None:
                reference_image = frame
                if feature_tracker is not None:
                    feature_tracker.add_reference_image(frame)

            if feature_tracker is not None:
                homography = feature_tracker.get_homography(
                    frame, reference_homography)
            else:
                homography = None

            if homography is not None:
                draw_homography(reference_image, frame, homography)

            global IMG_COUNTER
            cv2.imshow('frame', frame)
            cv2.imwrite('img' + str(IMG_COUNTER) + '.png', frame)
            IMG_COUNTER +=  1
            key = cv2.waitKey(IMAGE_WAIT_TIME) & 0xFF
            if key == ord('q'):
                break

    if reference_homography is not None:
        global AVERAGE_ENDPOINT_ERROR
        print(
            "Average endpoint error",
            np.average(
                np.array(AVERAGE_ENDPOINT_ERROR)))

    global RUN_PROFILING
    if RUN_PROFILING:
        global DETECT_FEATURES_TIME
        global DETECTED_FEATURES_COUNT
        global MATCHED_FEATURES_COUNT
        global MATCHED_FEATURES_TIME
        global REJECTED_OUTLIERS_TIME
        global REJECTED_OUTLIERS_COUNT
        global HOMOGRAPHY_TIME

        print(
            "Detected features count: ",
            np.average(
                np.array(DETECTED_FEATURES_COUNT)))
        print(
            "Detected features time: ",
            np.average(
                np.array(DETECT_FEATURES_TIME)))
        print(
            "Matched features count: ",
            np.average(
                np.array(MATCHED_FEATURES_COUNT)))
        print(
            "Matched features time: ",
            np.average(
                np.array(MATCHED_FEATURES_TIME)))
        print(
            "Rejected outliers count: ",
            np.average(
                np.array(REJECTED_OUTLIERS_COUNT)))
        print(
            "Rejected outliers time: ",
            np.average(
                np.array(REJECTED_OUTLIERS_TIME)))
        print("Homography time: ", np.average(np.array(HOMOGRAPHY_TIME)))


def camera_loop(
        save_image_filename,
        save_dir,
        reference_image_filename,
        descriptor_name,
        matcher_name):
    """Function that reads from the camera and processes them

    Args:
        save_image_filename (str): Save name for the single image save.
        save_dir (str): Save directory for the save image functionality.
        reference_image_filename (str): Filename for the reference image.
        descriptor_name (str): Name of the descriptor to be used.
        matcher_name (str): Name of the matcher to be used.
    """
    reference_image = get_reference_image(reference_image_filename)

    cap = cv2.VideoCapture(0)

    # Check and create directory if needed
    if save_dir:
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)

    if (descriptor_name != "none") and (matcher_name != "none"):
        feature_tracker = FeatureTracker(descriptor_name, matcher_name)
    else:
        feature_tracker = None

    if (reference_image is not None) and (feature_tracker is not None):
        feature_tracker.add_reference_image(reference_image)

    frame_count = 0
    while True:
        # Capture frame-by-frame
        _, frame = cap.read()

        if reference_image is None:
            reference_image = frame
            if feature_tracker is not None:
                feature_tracker.add_reference_image(frame)

        if save_dir:
            cv2.imwrite(
                os.path.join(
                    save_dir,
                    "img_" +
                    str(frame_count) +
                    ".png"),
                frame)

        if feature_tracker is not None:
            homography = feature_tracker.get_homography(frame, None)
        else:
            homography = None

        if homography is not None:
            draw_homography(reference_image, frame, homography)

        # Display the resulting frame
        cv2.imshow('frame', frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            break

        if key == ord(" "):
            if save_image_filename:
                cv2.imwrite(save_image_filename, frame)
            else:
                print("-c not used, unable to save file")

            reference_image = frame
            if feature_tracker is not None:
                feature_tracker.add_reference_image(frame)

        if save_dir is not None:
            with open(os.path.join(save_dir, save_dir + ".txt"), "a") as name_file:
                name_file.write(
                    os.path.join(
                        pathlib.Path().absolute(),
                        save_dir,
                        "img_" +
                        str(frame_count) +
                        ".png") +
                    "\n")

            frame_count = frame_count + 1

    global RUN_PROFILING
    if RUN_PROFILING:
        global DETECT_FEATURES_TIME
        global DETECTED_FEATURES_COUNT
        global MATCHED_FEATURES_COUNT
        global MATCHED_FEATURES_TIME
        global REJECTED_OUTLIERS_TIME
        global REJECTED_OUTLIERS_COUNT
        global HOMOGRAPHY_TIME

        print(
            "Detected features count: ",
            np.average(
                np.array(DETECTED_FEATURES_COUNT)))
        print(
            "Detected features time: ",
            np.average(
                np.array(DETECT_FEATURES_TIME)))
        print(
            "Matched features count: ",
            np.average(
                np.array(MATCHED_FEATURES_COUNT)))
        print(
            "Matched features time: ",
            np.average(
                np.array(MATCHED_FEATURES_TIME)))
        print(
            "Rejected outliers count: ",
            np.average(
                np.array(REJECTED_OUTLIERS_COUNT)))
        print(
            "Rejected outliers time: ",
            np.average(
                np.array(REJECTED_OUTLIERS_TIME)))
        print("Homography time: ", np.average(np.array(HOMOGRAPHY_TIME)))

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='This program demonstrates a several image-based segmentation algorithms.')
    parser.add_argument(
        '--save_filename',
        '-c',
        type=str,
        help='When the spacebar is pressed the image latest image will be saved to this file')
    parser.add_argument(
        '--save_dir',
        '-r',
        type=str,
        help='When capturing the program will save all the images to this directory with the corresponding route')
    parser.add_argument(
        '--reference_image', '-f',
        type=str,
        help='Image to perform the comparision against')
    parser.add_argument(
        '--camera_capture', '-v',
        help='Run the algorithm on images directly from the camera',
        action="store_true")
    parser.add_argument(
        '--image_names_file', '-t',
        type=str,
        help='Run the algorithm on files specified in the given text file')
    parser.add_argument(
        '--descriptor', '-d',
        choices=["sift", "akaze", "none"],
        default="sift",
        help='Select an option for the descriptor')
    parser.add_argument(
        '--matcher', '-m',
        choices=["bruteforce", "flann", "none"],
        default="bruteforce",
        help='Select an option for the matcher')
    parser.add_argument(
        '--run_test',
        type=str,
        help='Runs the test based on a series of homographies specified by the given file. Only works in file mode')
    parser.add_argument(
        '--run_profiling',
        help='Runs the profiling mode',
        action="store_true")
    args = parser.parse_args()

    if args.run_profiling:
        RUN_PROFILING = True

    if args.camera_capture:
        camera_loop(
            args.save_filename,
            args.save_dir,
            args.reference_image,
            args.descriptor,
            args.matcher)

    if args.image_names_file:
        directory_read(args.image_names_file, args.reference_image,
                       args.descriptor,
                       args.matcher,
                       args.run_test)

    if not args.camera_capture and not args.image_names_file:
        parser.print_help(sys.stderr)
