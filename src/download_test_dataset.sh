#!/bin/bash

mkdir -p dataset
cd dataset

for value in bikes trees graf wall bark boat leuven ubc
do
    mkdir -p $value
    cd $value
    touch $value.txt # Ensure the file exists
    : > $value.txt # Empty the file if it exists
    touch homography.txt
    : > homography.txt
    wget -c https://www.robots.ox.ac.uk/~vgg/research/affine/det_eval_files/$value.tar.gz
    tar xf $value.tar.gz
    echo "$(pwd)/img2.ppm" >> $value.txt
    echo "$(pwd)/img3.ppm" >> $value.txt
    echo "$(pwd)/img4.ppm" >> $value.txt
    echo "$(pwd)/img5.ppm" >> $value.txt
    echo "$(pwd)/img6.ppm" >> $value.txt

    echo "$(pwd)/H1to2p" >> homography.txt
    echo "$(pwd)/H1to3p" >> homography.txt
    echo "$(pwd)/H1to4p" >> homography.txt
    echo "$(pwd)/H1to5p" >> homography.txt
    echo "$(pwd)/H1to6p" >> homography.txt

    cd ..
done

cd ..
